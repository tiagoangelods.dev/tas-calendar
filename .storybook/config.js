import { configure } from '@storybook/react';
import '!style-loader!css-loader!@material/button/dist/mdc.button.min.css';
import '!style-loader!css-loader!@material/icon-button/dist/mdc.icon-button.min.css';
import '!style-loader!css-loader!@material/textfield/dist/mdc.textfield.css'; //text-field css
import '!style-loader!css-loader!@material/floating-label/dist/mdc.floating-label.css'; //text-field css dependency
import '!style-loader!css-loader!@material/notched-outline/dist/mdc.notched-outline.css'; //text-field css dependency
import '!style-loader!css-loader!@material/line-ripple/dist/mdc.line-ripple.css'; //text-field css dependency
import '!style-loader!css-loader!../src/assets/styles/index.css';

const stories = require.context('../stories', true, /\.js$/);

function loadStories() {
  stories.keys().forEach(stories);
}

configure(loadStories, module);
