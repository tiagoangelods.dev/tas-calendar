import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import CustomButton from '../src/components/common/CustomButton';

storiesOf('CustomButton', module)
  .add('default', () => <CustomButton text="button" onClick={action('clicked')} />)
  .add('disabled', () => <CustomButton text="button" disabled onClick={action('clicked')} />);