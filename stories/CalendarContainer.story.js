import React from 'react';
import { storiesOf } from '@storybook/react';
import CalendarContainer from '../src/components/calendar/CalendarContainer';

storiesOf('CalendarContainer', module)
  .add('month view', () => <CalendarContainer type={0} />)
  .add('week view', () => <CalendarContainer type={1} />);