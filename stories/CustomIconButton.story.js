import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import CustomIconButton from '../src/components/common/CustomIconButton';

storiesOf('CustomIconButton', module)
  .add('default icon="star"', () => <CustomIconButton icon="star" onClick={action('clicked')} />)
  .add('icon url', () => 
    <CustomIconButton icon="https://www2.le.ac.uk/departments/law/images/twitter-follow-us-icon" onClick={action('clicked')} />
  );