import React from 'react';
import { storiesOf } from '@storybook/react';
import CustomTextField from '../src/components/common/CustomTextField';

storiesOf('CustomTextField', module)
  .add('default label', () => <CustomTextField label="text field label" />);