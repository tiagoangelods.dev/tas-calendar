# CALENDAR

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

this project was created with Reactjs 16.0.0

## browse support

![Chrome](https://raw.github.com/alrra/browser-logos/master/src/chrome/chrome_48x48.png) | ![Firefox](https://raw.github.com/alrra/browser-logos/master/src/firefox/firefox_48x48.png) | ![Edge](https://raw.github.com/alrra/browser-logos/master/src/edge/edge_48x48.png) | ![Safari](https://raw.github.com/alrra/browser-logos/master/src/safari/safari_48x48.png)
--- | --- | --- | --- |
Latest ✔ | Latest ✔ | Latest ✔ | Latest ✔ |

## running and testing

for run this project you must follow the steps below:

* clone this project on your pc
 `git clone https://gitlab.com/tiagoangelods.dev/tas-calendar.git`

* run `npm install`

* run `npm start`

* visit [localhost](http://localhost:3000) on port 3000

for test after run `npm install`

  * run `npm run test`

for only view the components behavior separatedly after npm install:

  * run `npm run storybook`

  * visit [localhost](http://localhost:9001) on port 9001

  * [WIP] some stories was broken after install redux and put provider on project

## What was used to develop this project

- React, JSX and ES6 support with Babel.
- A dev server with live reload
- Stack for tests
- Linters

#### Environment

[React Storybook](https://github.com/kadirahq/react-storybook) - *to tell stories with different behaviors of the component and provide an example page.*

#### Style Guide

- [EditorConfig](http://editorconfig.org/) - *standardize some general settings among multiple editors*
- [ESLint](http://eslint.org/) - *for reporting the patterns of code*
  - [Airbnb JavaScript Style Guide](https://github.com/airbnb/javascript)
  - **Plugins**
    - [React](https://github.com/yannickcr/eslint-plugin-react)
    - [A11y](https://github.com/evcohen/eslint-plugin-jsx-a11y)
    - [Import](https://github.com/benmosher/eslint-plugin-import)

#### Tests
- [Mocha](https://github.com/mochajs/mocha) - *test framework*
- [Chai](https://github.com/chaijs/chai) - *assertions*
- [Enzyme](https://github.com/airbnb/enzyme) - *shallow component*
- [Jsdom](https://github.com/tmpvar/jsdom) - *mock the browser*

#### Compiler

- [babel](https://babeljs.io/)
  - **Plugins**
    - [ES2015](https://www.npmjs.com/package/babel-preset-es2015)
    - [React](https://www.npmjs.com/package/babel-preset-react)

## Code Standards

This project uses [eslint](http://eslint.org/) and [.editorconfig](http://editorconfig.org/) is defined to have indent_size of **2 spaces**. You can change that on [.eslintrc]() file.

This project also uses [Husky](https://github.com/typicode/husky) to prevent to push code with lint errors or broken tests. If you don't want this, you can uninstall running `npm uninstall --save-dev husky` and deleting the [prepush command]()) on `package.json`.

## License

[MIT License]() @ [tiagoangelods](https://gitlab.com/tiagoangelods.dev)
