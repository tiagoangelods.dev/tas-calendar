require('../enzyme.config');
import React from 'react';
import { render } from 'enzyme';
import { expect } from 'chai';
import CustomButton from '../src/components/common/CustomButton';

describe('CustomButton', () => {
  it('should render without crash', () => {
    const wrapper = render(<CustomButton text="button" />);
    expect(wrapper).to.have.property("text");
    
  });
});