require('../enzyme.config');
import React from 'react';
import { render } from 'enzyme';
import { expect } from 'chai';
import CustomTextField from '../src/components/common/CustomTextField';

describe('CustomTextField', () => {
  it('should render without crash', () => {
    const wrapper = render(<CustomTextField label="text" />);
    expect(wrapper.children()[0]).to.have.property('name').equal('input');
  });
});