import { expect } from 'chai';
import { firstLastDay, weekDays, months, getStartOfWeek, getEndOfWeek } from '../src/utils/_dateHelpers';

describe('Date Helpers', () => {
  it('should show de first and last date number of current month', () => {
    const date = new Date();
    const currentFirstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    const currentLastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    const { firstDay, lastDay } = firstLastDay();

    expect(firstDay.getDate()).to.be.equal(currentFirstDay.getDate());
    expect(lastDay.getDate()).to.be.equal(currentLastDay.getDate());
  });

  it('should show 1987 as year', () => {
    const { firstDay } = firstLastDay({ year: 1987 });
    expect(firstDay.getFullYear()).to.be.equal(1987);
  });

  it('should show 9 as month', () => {
    const { firstDay } = firstLastDay({ month: 9 });
    expect(firstDay.getMonth()).to.be.equal(9);
  });

  it('should show 1 as date', () => {
    const { firstDay } = firstLastDay();
    expect(firstDay.getDate()).to.be.equal(1);
  });

  it('should week to be length 7', () => {
    expect(weekDays).to.have.length(7);
  });

  it('should months to be length 12', () => {
    expect(months).to.have.length(12);
  });

  it('should show the first day of the week', () => {
    const date = new Date(2019, 0, 19);
    const sunday = getStartOfWeek(date);
    expect(sunday.getDate()).to.be.equal(13);
  });

  it('should show the last day of the week', () => {
    const date = new Date(2019, 0, 15);
    const saturday = getEndOfWeek(date);
    expect(saturday.getDate()).to.be.equal(19);
  });
});