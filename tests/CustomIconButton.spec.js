require('../enzyme.config');
import React from 'react';
import { render } from 'enzyme';
import { expect } from 'chai';
import CustomIconButton from '../src/components/common/CustomIconButton';

describe('CustomIconButton', () => {
  it('should render without crash', () => {
    const wrapper = render(<CustomIconButton icon="star" />);
    expect(wrapper.children()).to.have.length(1);
  });
});