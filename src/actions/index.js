export const NAVIGATION_ACTION = 'NAVIGATION_ACTION';
export const LOAD_EVENTS = 'LOAD_EVENTS';
export const SAVE_EVENT = 'SAVE_EVENT';
export const DELETE_EVENT = 'DELETE_EVENT';

export const navigate = ({ year, month, viewType, startOfWeek, endOfWeek }) => ({
  type: 'NAVIGATION_ACTION',
  payload: { year, month, viewType, startOfWeek, endOfWeek }
});

export const loadEvents = () => ({
  type: 'LOAD_EVENTS'
});

export const saveEventAction = ({ event }) => ({
  type: 'SAVE_EVENT',
  payload: { event }
});

export const deleteEventAction = ({ event }) => ({
  type: 'DELETE_EVENT',
  payload: { event }
});