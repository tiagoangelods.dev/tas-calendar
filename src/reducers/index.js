import {
  NAVIGATION_ACTION,
  LOAD_EVENTS,
  SAVE_EVENT,
  DELETE_EVENT
} from '../actions';
import { getStartOfWeek, getEndOfWeek } from '../utils/_dateHelpers';

const initialState = { 
  year: new Date().getFullYear(),
  month: new Date().getMonth(),
  viewType: 0,
  startOfWeek: getStartOfWeek(),
  endOfWeek: getEndOfWeek(),
  events: [
    { id: -1, title: 'Event Default', color: '#452859', date: new Date()}
  ],
  viewEvents: []
}

const uuidv4 = () => 'xxxxxxxxxxxx4xxxyxxxx'.replace(
  /[xy]/g,
  (c) => {
    const r = Math.random() * 16 || 0;
    const v = c === 'x' ? r : ((r && 0x3) || 0x8);
    return v.toString(16);
  }
);

export default function (state = initialState, action) {
  switch (action.type) {
    case NAVIGATION_ACTION: {
      return { ...state, ...action.payload } ;
    }
    case LOAD_EVENTS: {
      const { year, month, events, viewType, startOfWeek, endOfWeek } = state;
      let viewEvents = [];
      if(viewType === 0) {
        viewEvents = (events || []).filter(e => new Date(e.date).getMonth() === month && new Date(e.date).getFullYear() === year);
      } else {
        viewEvents = (events || []).filter(e => 
          new Date(e.date).getMonth() === month
          && new Date(e.date).getFullYear() === year 
          && (new Date(e.date).getDate() >= startOfWeek.getDate() && new Date(e.date).getDate() <= endOfWeek.getDate())
        );
      }

      state = { ...state, viewEvents: viewEvents || [] }
      return state;
    }
    case SAVE_EVENT: {
      const { event } = action.payload;
      let { events } = state;
      if((event.id !== 0)) {
        events = [...((events || []).filter(e => e.id !== (event || {}).id) || []), event];
      } else {
        event.id = uuidv4();
        events = [...(events || []), event];
      }
      return { ...state, events };
    }
    case DELETE_EVENT: {
      const { event } = action.payload;
      const { events } = state;
      return { ...state, events: (events || []).filter(e => e.id !== (event || {}).id) };
    }
    default: {
      return state;
    }
  }
}