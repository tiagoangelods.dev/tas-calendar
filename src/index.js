/* eslint-disable no-unused-vars */
import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import reducer from './reducers';
import { Provider } from 'react-redux';
import '@material/button/dist/mdc.button.min.css'; //button css
import '@material/icon-button/dist/mdc.icon-button.min.css'; //icon-button css
import '@material/textfield/dist/mdc.textfield.css'; //text-field css
import '@material/floating-label/dist/mdc.floating-label.css'; //text-field css dependency
import '@material/notched-outline/dist/mdc.notched-outline.css'; //text-field css dependency
import '@material/line-ripple/dist/mdc.line-ripple.css'; //text-field css dependency
import '@material/dialog/dist/mdc.dialog.css'; //dialog
import './assets/styles/index.css'; //main css
import App from './App';
import * as serviceWorker from './serviceWorker';

const store = createStore(
  reducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
