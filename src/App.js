/* eslint-disable no-unused-vars */
import React from 'react';
import CalendarNavigationBar from './components/calendar/CalendarNavigationBar';
import CalendarContainer from './components/calendar/CalendarContainer';


class App extends React.Component {
  render() {
    return (
      <div className="app">
        <div className="app-header">
          <CalendarNavigationBar />
        </div>
        <div className="app-body">
          <CalendarContainer />
        </div>
      </div>
    );
  }
}

export default App;
