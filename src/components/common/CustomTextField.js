import React from 'react';
import { TextField } from '@rmwc/textfield';

const CustomTextField = ({ ...props }) => <TextField {...props} />;

export default CustomTextField;
