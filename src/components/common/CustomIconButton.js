import React from 'react';
import { IconButton } from '@rmwc/icon-button';

const CustomIconButton = ({ icon, ...props }) => <IconButton icon={icon} {...props} />;

export default CustomIconButton;
