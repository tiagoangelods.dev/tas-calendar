import React from 'react';
import { Button } from '@rmwc/button';

const CustomButton = ({ text, active, ...props }) => <Button className={`custom-button ${(active) ? 'active-button' : ''}`} {...props}>{text}</Button>;

export default CustomButton;
