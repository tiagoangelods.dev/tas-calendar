import React, { Component } from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  DialogButton
} from '@rmwc/dialog';
import CustomTextField from '../common/CustomTextField';
import { formatDate } from '../../utils/_dateHelpers';

class CalendarDialog extends Component {
  render() {
    const {
      isOpen,
      closeDialog,
      event,
      changeTitle,
      changeDate,
      changeColor,
      saveEvent
    } = this.props;
    return (
      <Dialog open={isOpen}>    
        <DialogTitle>New Reminder</DialogTitle>
        <DialogContent className="reminder-dialog" style={{ display: 'flex', flexDirection: 'column' }}>
          <CustomTextField
            label="Reminder title"
            value={event.title}
            maxLength={30}
            onChange={(evt) => changeTitle(evt.target.value)} />
          <CustomTextField
            label="Reminder datetime"
            type="datetime-local"
            value={formatDate(event.date)}
            onChange={(evt) => changeDate(evt.target.value)} />
          <CustomTextField
            label="Reminder color"
            type="color"
            style={{width: '6rem'}}
            value={event.color}
            onChange={(evt) => changeColor(evt.target.value)} />
        </DialogContent>
        <DialogActions>
          <DialogButton onClick={() => closeDialog()}>Cancel</DialogButton>
          <DialogButton
            isDefaultAction
            onClick={() => saveEvent(event)}>
              Save
          </DialogButton>
        </DialogActions>
      </Dialog>
    )
  }
};

export default CalendarDialog;