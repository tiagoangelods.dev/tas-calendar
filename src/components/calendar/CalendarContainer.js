import React, { Component } from 'react';
import { connect } from 'react-redux';
import { weekDays, firstLastDay } from '../../utils/_dateHelpers';
import CalendarDialog from './CalendarDialog';
import CustomIconButton from '../common/CustomIconButton';
import { loadEvents, deleteEventAction, saveEventAction } from '../../actions';

const MonthView = ({ dayOne, lastDay, events, editFunction = function(){}, deleteFunction = function(){} }) => {
  const month = [];
  let day = [];
  const dayInMonth = new Date(dayOne).getDay();
  let firstDay = { count: 0, weekday: weekDays[dayInMonth].name, date: null }
  const totalDays = new Date(lastDay).getDate();
  let eventList = [];

  for(let row = 0; row < 6; row++){
    day = [];
    for(let col = 0; col < 7; col++) {
      eventList = [];
      if (firstDay.weekday === weekDays[col].name && row === 0) {
        firstDay.count = new Date(dayOne).getDate();
        firstDay.date = new Date(dayOne);
      } else if (firstDay.count > 0 && firstDay.count <= totalDays) {
        firstDay.date = new Date(firstDay.date);
        firstDay.date.setDate(firstDay.date.getDate() + 1);
        firstDay.count = firstDay.date.getDate();
      }

      if(firstDay.date !== null && firstDay.date <= new Date(lastDay))
        eventList = selectEvents(firstDay.date, events, editFunction, deleteFunction);
      
      day.push(
        React.createElement(
          'div',
          {
            key:`weekday${row}_${col}`,
            className: `calendar-week-day ${weekDays[col].shortName}`,
            weekday: weekDays[col].shortName
          },
          [
            React.createElement(
              'div',
              {
                key:`weekday-title${row}_${col}`,
                className: `week-day-title ${weekDays[col].shortName}`,
                weekday: weekDays[col].shortName,
                'date-value': (firstDay.date !== null && firstDay.date <= new Date(lastDay)) ? firstDay.date : ''
              },
              [
                React.createElement(
                  'div',
                  {
                    key:`weekday-title-name${row}_${col}`,
                    className: 'weekday-title-name',
                    'date-value': (firstDay.date !== null && firstDay.date <= new Date(lastDay)) ? firstDay.date : ''
                  },
                  (row === 0) && weekDays[col].name
                ),
                React.createElement(
                  'div',
                  {
                    key:`weekday-title-day${row}_${col}`,
                    className: 'weekday-title-day',
                    'date-value': (firstDay.date !== null && firstDay.date <= new Date(lastDay)) ? firstDay.date : ''
                  },
                  (((firstDay.count > 0 && firstDay.count < totalDays) || firstDay.count === totalDays) && firstDay.count)
                )
              ]
            ),
            React.createElement(
              'div',
              {
                key:`weekday-day${row}_${col}`,
                className: `week-day-content ${weekDays[col].shortName}`,
                weekday: weekDays[col].shortName,
                'date-value': (firstDay.date !== null && firstDay.date <= new Date(lastDay)) ? firstDay.date : ''
              },
              eventList
            )
          ]
        )
      );
      if (firstDay.count === totalDays) {
        firstDay.count = 0;
        firstDay.date = null;
      }
    }
    month.push(
      React.createElement(
        'div',
        {
          key: `week${row}`,
          className: 'calendar-week'
        },
        [...day]
      )
    );
  }

  return month;
}

const WeekView = ({ dayOne, events, editFunction = function(){}, deleteFunction = function(){} }) => {
  const week = [];
  let firstDay = new Date(dayOne);
  let eventList = [];

  for(let col = 0; col < 7; col++) {
    eventList = [];
    eventList = selectEvents(new Date(firstDay), events, editFunction, deleteFunction);
    week.push(
      React.createElement(
        'div',
        {
          key: `week${col}`,
          className: `calendar-week-day ${weekDays[col].shortName}`,
          weekday: weekDays[col].shortName
        },
        [
          React.createElement(
            'div',
            {
              key: `week-title${col}`,
              className: `week-day-title ${weekDays[col].shortName}`,
              weekday: weekDays[col].shortName,
              'date-value': new Date(firstDay)
            },
            [
              React.createElement(
                'div',
                {
                  key:`weekday-title-name${col}`,
                  className: 'weekday-title-name',
                  'date-value': new Date(firstDay)
                },
                weekDays[col].name
              ),
              React.createElement(
                'div',
                {
                  key:`weekday-title-day${col}`,
                  className: 'weekday-title-day',
                  'date-value': new Date(firstDay)
                },
                firstDay.getDate()
              )
            ]
          ),
          React.createElement(
            'div',
            {
              key: `week-day${col}`,
              className: `week-day-content ${weekDays[col].shortName}`,
              weekday: weekDays[col].shortName,
              'date-value': new Date(firstDay)
            },
            eventList
          )
        ]
      )
    );
    firstDay.setDate(firstDay.getDate()+1);
  }
    

  return React.createElement(
    'div',
    {
      className: 'calendar-week-view'
    },
    [...week]
  );
}

const EventView = ({ event, editItem = function(){}, deleteItem = function(){} }) => {
  return (
    <div className="event-view" date-value={new Date(event.date)}>
      <div className="event-flag" style={{ backgroundColor: event.color }} date-value={new Date(event.date)} />
      <div className="event-content" date-value={new Date(event.date)}>
        <div>
          <div className="event-title" date-value={new Date(event.date)}>{event.title}</div>
          <div className="event-date" date-value={new Date(event.date)}>{new Date(event.date).toLocaleString()}</div>
        </div>
        <div className="event-options">
          <CustomIconButton label="edit" icon="edit" onClick={() => editItem(event)}/>
          <CustomIconButton label="delete" icon="delete" onClick={() => deleteItem(event)} />
        </div>
      </div>
    </div>
  );
}

const selectEvents = (date, events, editFunction = function(){}, deleteFunction = function(){}) => {
  const dateEvent = new Date(date).toLocaleDateString();
  let eventsDate = (events || []).filter(e => new Date(e.date).toLocaleDateString() === dateEvent);
  if((eventsDate || []).length) {
    eventsDate = eventsDate.sort(function(a,b){
      return new Date(a.date) - new Date(b.date);
    });
  }
  
  return (eventsDate || []).map(e => 
    (
      <EventView
        key={`evento${e.id}`}
        event={e}
        editItem={editFunction}
        deleteItem={deleteFunction} />
    )
  ) || [];
}

class CalendarContainer extends Component {
  constructor(){
    super();
    this.state = {
      firstDay: firstLastDay().firstDay,
      lastDay: firstLastDay().lastDay,
      openDialog: false,
      event: {
        id: 0,
        date: new Date(),
        color: '#ffffff',
        title: ''
      }
    }
  }

  componentWillReceiveProps(nextProps){
    const { year, month, viewType, startOfWeek } = nextProps;
    if (viewType === 0) {
      const { firstDay, lastDay } = firstLastDay({ month, year });
      this.setState({ firstDay, lastDay });
    } else {
      const firstDay = new Date(startOfWeek);
      const { lastDay } = firstLastDay({ year: firstDay.getFullYear(), month: firstDay.getMonth() });
      this.setState({ firstDay, lastDay });
    }
  }

  componentDidMount() {
    this.props.dispatch(loadEvents());
    Array.from(document.getElementsByClassName("week-day-title")).forEach(el => {
      el.addEventListener('click', this.displayDate);
    });
    Array.from(document.getElementsByClassName("week-day-content")).forEach((element) => {
      element.addEventListener('click', this.displayDate);
    });
    Array.from(document.getElementsByClassName("event-view")).forEach((element) => {
      element.removeEventListener('click', this.displayDate);
    });
  }

  componentDidUpdate() {
    Array.from(document.getElementsByClassName("week-day-title")).forEach(el => {
      el.addEventListener('click', this.displayDate);
    });
    Array.from(document.getElementsByClassName("week-day-content")).forEach((element) => {
      element.addEventListener('click', this.displayDate);
    });
    Array.from(document.getElementsByClassName("event-view")).forEach((element) => {
      element.removeEventListener('click', this.displayDate);
    });
  }

  editEvent = (event) => {
    this.setState({ event, openDialog: true });
  }

  deleteEvent = (event) => {
    this.props.dispatch(deleteEventAction({ event }));
    this.props.dispatch(loadEvents());
  }

  displayDate = (e) => {
    if(e.target.attributes['date-value']) {
      const date = e.target.attributes['date-value'].value;
      const { event } = this.state;
      if ( date !== '') {
        const nEvent = { ...event, date: new Date(date) };
        this.setState({ event: nEvent, openDialog: true });
      }
    }
  }

  changeDate = (date) => {
    const { event } = this.state;
    if(date !== undefined && date !== null) {
      const nEvent = { ...event, date: new Date(date) };
      this.setState({ event: nEvent, openDialog: true });
    }
  }

  changeTitle = (title) => {
    const { event } = this.state;
    const nEvent = { ...event, title };
    this.setState({ event: nEvent });
  }

  changeColor = (color) => {
    const { event } = this.state;
    const nEvent = { ...event, color };
    this.setState({ event: nEvent });
  }

  closeDialog() {
    this.setState({ openDialog: false, event: { id: 0, title: '', color: '#ffffff', date: new Date()} });
  }

  saveEvent = (event) => {
    if((event.title || '').length) {
      this.props.dispatch(saveEventAction({ event }));
      this.props.dispatch(loadEvents());
      this.closeDialog();
    } else {
      alert('Event title is required');
    }
  }

  render() {
    const { firstDay, lastDay, openDialog, event } = this.state;
    const { viewType, viewEvents } = this.props;
    return (
      <div>
        <CalendarDialog
          isOpen={openDialog}
          event={event}
          changeTitle={this.changeTitle}
          changeDate={this.changeDate}
          changeColor={this.changeColor}
          saveEvent={this.saveEvent}
          closeDialog={() => this.closeDialog()} />        
        <div className="calendar-container">
          {(viewType === 0) && (
            <MonthView
              dayOne={firstDay}
              lastDay={lastDay}
              events={viewEvents}
              editFunction={this.editEvent}
              deleteFunction={this.deleteEvent} />
          )}
          {(viewType === 1) && (
            <WeekView
              dayOne={firstDay}
              events={viewEvents}
              editFunction={this.editEvent}
              deleteFunction={this.deleteEvent} />
          )}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return state;
}

export default connect(mapStateToProps)(CalendarContainer);