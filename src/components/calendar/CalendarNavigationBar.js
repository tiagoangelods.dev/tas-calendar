import React, { Component } from 'react';
import { connect } from 'react-redux';
import { navigate, loadEvents } from '../../actions';
import CustomButton from '../common/CustomButton';
import CustomIconButton from '../common/CustomIconButton';
import { months, getStartOfWeek, getEndOfWeek } from '../../utils/_dateHelpers';

class CalendarNavigationBar extends Component {
  changeVisualizationType = (value) => {
    const { year, month, startOfWeek, endOfWeek } = this.props;
    this.props.dispatch(navigate({ year, month, viewType: value, startOfWeek, endOfWeek }));
    this.props.dispatch(loadEvents());
  }

  setToday() {
    const { viewType } = this.props;
    const today = new Date();
    const sunday = getStartOfWeek(today);
    const saturday = getEndOfWeek(today);
    this.props.dispatch(navigate({ year: today.getFullYear(), month: today.getMonth(), viewType, startOfWeek: sunday, endOfWeek: saturday }));
    this.props.dispatch(loadEvents());
  }

  prevNextClick(dir, type) {
    const { year, month, startOfWeek, endOfWeek } = this.props;
    let cDate = new Date();
    let newYear = year;
    let newMonth = month;
    let newStartOfWeek = startOfWeek;
    let newEndOfWeek = endOfWeek;

    if(dir === 'prev') {
      if(type === 0) {
        if(month === 0) {
          newYear = year - 1;
          newMonth = 11;
          cDate = new Date(newYear, newMonth, 1);
        } else {
          newMonth = month - 1;
          cDate = new Date(newYear, newMonth, 1);
        }

        newStartOfWeek = getStartOfWeek(cDate);
        newEndOfWeek = getEndOfWeek(cDate);
      } else {
        cDate = new Date(newYear, newMonth, newStartOfWeek.getDate());
        cDate.setDate(cDate.getDate() - 7);
        newStartOfWeek = getStartOfWeek(cDate);
        newEndOfWeek = getEndOfWeek(cDate);
        newYear = newStartOfWeek.getFullYear();
        newMonth = newStartOfWeek.getMonth();
      }
    } else {
      if(type === 0) {
        if(month === 11) {
          newYear = year + 1;
          newMonth = 0;
          cDate = new Date(newYear, newMonth, 1);
        } else {
          newMonth = month + 1;
          cDate = new Date(newYear, newMonth, 1);
        }
        newStartOfWeek = getStartOfWeek(cDate);
        newEndOfWeek = getEndOfWeek(cDate);
      } else {
        cDate = new Date(newYear, newMonth, newEndOfWeek.getDate());
        cDate.setDate(cDate.getDate() + 7);
        newStartOfWeek = getStartOfWeek(cDate);
        newEndOfWeek = getEndOfWeek(cDate);
        newYear = newEndOfWeek.getFullYear();
        newMonth = newEndOfWeek.getMonth();
      }
    }

    this.props.dispatch(navigate({ year: newYear, month: newMonth, viewType: type, startOfWeek: newStartOfWeek, endOfWeek: newEndOfWeek }));
    this.props.dispatch(loadEvents());
  }

  render(){
    const { year, month, viewType, startOfWeek, endOfWeek } = this.props;
    return (
      <div className="calendar_navigation_bar">
        <div className="calendar_navigation_controls">
          <CustomIconButton label="prev" icon="chevron_left" onClick={() => this.prevNextClick('prev', viewType)} />
          <CustomIconButton label="next" icon="chevron_right" onClick={() => this.prevNextClick('next', viewType)} />
          <div className="calendar_month_year_controls">
            {(viewType === 0) && `${months[month].name} ${year}`}
            {(viewType === 1) && `${months[startOfWeek.getMonth()].shortName} ${startOfWeek.getDate()} to ${months[endOfWeek.getMonth()].shortName} ${endOfWeek.getDate()}`}
          </div>
        </div>
        <div className="calendar_visualization_controls">
          <CustomButton active={(viewType === 0)} text="Month" onClick={() => this.changeVisualizationType(0)} />
          <CustomButton active={(viewType === 1)} text="Week" onClick={() => this.changeVisualizationType(1)} />
          <CustomButton text="Today" onClick={() => this.setToday()} />
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return state;
}

export default connect(mapStateToProps)(CalendarNavigationBar);