export const weekDays = [
  { name: 'Sunday', shortName: 'Sun' },
  { name: 'Monday', shortName: 'Mon' },
  { name: 'Tuesday', shortName: 'Tue' },
  { name: 'Wednesday', shortName: 'Wed' },
  { name: 'Thursday', shortName: 'Thu' },
  { name: 'Friday', shortName: 'Fri' },
  { name: 'Saturday', shortName: 'Sat' }
];

export const months = [
  { name: 'January', shortName: 'Jan' },
  { name: 'February', shortName: 'Feb' },
  { name: 'March', shortName: 'Mar' },
  { name: 'April', shortName: 'Apr' },
  { name: 'May', shortName: 'May' },
  { name: 'June', shortName: 'Jun' },
  { name: 'July', shortName: 'Jul' },
  { name: 'August', shortName: 'Aug' },
  { name: 'September', shortName: 'Sep' },
  { name: 'October', shortName: 'Oct' },
  { name: 'November', shortName: 'Nov' },
  { name: 'December', shortName: 'Dec' }
];

export const firstLastDay = ({ month, year } = {}) => {
  
  (month === null || month === undefined) && (month = new Date().getMonth());
  (year === null || year === undefined) && (year = new Date().getFullYear());

  const date = new Date(year, month);
  const firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
  const lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
  return { firstDay, lastDay };
}

export const getStartOfWeek = (date = new Date()) => {
  const diff = date.getDate() - date.getDay() + (date.getDay() === 0 ? -date.getDay() : 0);

  return new Date(date.setDate(diff));
}

export const getEndOfWeek = (date = new Date()) => {
  const startOfWeek = getStartOfWeek(date);
  const endOfWeek = date.setDate(startOfWeek.getDate() + 6);

  return new Date(endOfWeek);
}

export const formatDate = (date) => {
  if (!isNaN(new Date(date))) {
    return new Date(new Date(date).toString().split('GMT')[0]+' UTC').toISOString().split('.')[0];
  }
  return new Date(new Date().toString().split('GMT')[0]+' UTC').toISOString().split('.')[0]
}